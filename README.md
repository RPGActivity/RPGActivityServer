# RPGActivityServer

A fediverse instance extended to support a 
 roleplaying game. A Multi-User Dungeon.

Every account is a player.

There is a shared map, that is displayed in the client.

There are activitypub actors to represent:

* Non-player characters

* Locales

* Crafted items

* Set Dressing

