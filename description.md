A fediverse instance extended to support a roleplaying game. A Multi-User Dungeon.

Every account is a player.

There is a shared map, that is displayed in the client.

There are activitypub actors to represent:

* Non-player characters
Bots, quest givers, plots and subplots. Mounts, monsters.
* Locales: A location and it's contents including other players, NPCs, portals to other locales, items that have their own behavior and that can interact with players.

* Crafted items, reside in a locale and interact with players the base behavior set for crafted items is pick up/set down, default trigger action, load personality, grant permissions, bless with permission tokens.

* Set Dressing: This is the visual furniture of a room. For accessibility reasons we always start with the description. Machine generated sets must be watermarked as such. From a UI standpoint it presents as a series of facades that is visually behind UI representing players, actors and items. This visual order may be presented as a text stream for people using a screen reader other line oriented output device.
So for visually impaired users a scene attached to a room will be described in the order players, actors, items, location, visible locations nearby.

Design choices.

* All players get a room that is theirs that they can invite other players to visit for a set time. Combat behaviors are disabled on all items in a personal room. 

* level maps are a series of interconnected rooms, there are no default constraints on the topology of the map although maps representing physical spaces will want to implement that constraint set.

* Player stats can be set at creation time but are immutable after that. Player inventory is mutable and records access tokens and consumables. 

* The default stats set will be Wit, Will, Wonder, Caring, Taste, and Tidy which correspond to the archetypes Mage, Warrior, Monk, Healer, Cook, and Broom

* Player demographics are mutable. Age is constrained and cannot start before 18 but once set progresses with the with the player.  Gender is represented as a HSL color space that the user can change when in a personal room. Hue representing gender identity, Saturation to represent arousal (horny), and brightness to represent the ace to ambi axis. This is represented to non-visual users using a standard spectrum description for hues using the keywords for the 16 basic colors in https://developer.mozilla.org/en-US/docs/Web/CSS/named-color

* Interaction rules are set at server creation. But the default set includes, sensibility, motion and ontological constraints but otherwise leaves it up to the players and the world building. Some interactions involve items that are Weapons and actions that are Combat. Weapons become inert in personal rooms. Combat actions can change other players inventory without their permission.
